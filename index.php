<!DOCTYPE html>
<html>
    <head>
    	<meta charset="utf-8"/>
        <title>Pequeños Maestros: Set!</title>
        <script src="jspsych-6.0.4/jspsych.js?v=2"></script>
        <script src="jspsych-6.0.4/plugins/jspsych-html-keyboard-response.js?v=1"></script>
        <script src="jspsych-6.0.4/plugins/jspsych-instructions.js?v=3"></script>
        <script src="jspsych-6.0.4/plugins/jspsych-fullscreen.js?v=1"></script>
        <script src="jspsych-6.0.4/plugins/jspsych-video.js?v=4"></script>
        <script src="jspsych-6.0.4/plugins/jspsych-audio-keyboard-response.js?v=1"></script>
        <script src="recordAudio.js?v=2"></script>
        <script type="text/javascript" src="stimuli.json?v=1"></script>
        <script src="auxiliary.js?v=3"></script>
        <script src="preload_images.js?v=1"></script>
        <link href="jspsych-6.0.4/css/jspsych.css" rel="stylesheet" type="text/css"></link>
    </head>
    <body oncontextmenu="return false;" ondragstart="return false;" onselectstart="return false;" style="cursor:none"></body>
    <script>

    	var tmin_instructions = 100
    	var timeout_instructions = 600
    	var timeout_game = 600
    	var timeout_known = 60
    	var video_decision_timeout = 30
    	var prewarn = 60
		var client_ip = '<?php echo $_SERVER['REMOTE_ADDR']; ?>'

    	for (gid=501; gid<=511; gid++) {
    		stimuli[gid-1] = {"retest": stimuli[0].retest}
    	}
    	for (mock_id=900; mock_id<=999; mock_id++) {
    		stimuli[mock_id-1] = stimuli[0];
    	}

    	function Timer(user_callback, delay, prewarn) {
    		var timerId, start, remaining = (delay-prewarn)*1000;
    		var callback = function() {
    			timerId = undefined;
    			var div = document.createElement('div')
    			div.id = 'timewarning'
    			div.style = 'position:absolute;top:10px;left:50vw;transform:translateX(-50%)'
    			div.innerHTML = `
    				<span style="background:#222;color:red;font-weight:bold;padding:5px;border-radius:5px">
    					QUEDA${Math.ceil(prewarn/60)>1 ? 'N' : ''} MENOS DE ${Math.ceil(prewarn/60)} MINUTO${Math.ceil(prewarn/60)>1 ? 'S' : ''}
					</span>
    				`;
    			document.body.appendChild(div)
    			remaining -= new Date() - start;
    			remaining += prewarn*1000;
    			callback = user_callback
    			resume()
    		}

		    var resume = function() {
		    	if (!timerId) {
			        start = new Date();
			        timerId = window.setTimeout(callback, remaining);
			        console.log('timer started');
			    };
		    };		    

		    this.pause = function() {
		        if (timerId) {
		        	this.clear();
			        remaining -= new Date() - start;
			        start = undefined;
			    };
		    };

		    this.background = function() {
		    	if (timerId) {
		    		console.log('timer to background');
		    		this.clear();		    		
		    	};
		    };

		    this.foreground = function() {
		    	if (!timerId) {
		    		remaining -= new Date() - start;
		    		console.log('timer to foreground');
		    		resume()
			    };
		    };

		    this.clear = function() {
		    	if (timerId) {
			    	window.clearTimeout(timerId);
			    	timerId = undefined;
			    	console.log('timer cleared')
			    };
		    };

		    resume();
		}
    	var timer = null

    	var removeTimeWarning = function() {
    		timewarning = document.getElementById('timewarning')
    		if (timewarning) {
    			timewarning.parentNode.removeChild(timewarning)
    		}
    	}

    	// Script tries to save the data when the winner screen is loaded and
    	// when the session is closed earlier with ctrl+alt+q.
    	// The script also tries to save the data if the page is tried to be unloaded,
    	// but in this case now js warnings are given if it fails, because this is not allowed
    	// in beforeunload listeners.

		var urlParams = new URLSearchParams(window.location.search)

		window.mobilecheck = function() {
			// https://stackoverflow.com/questions/11381673/detecting-a-mobile-browser
			var check = false;
			(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
			return check;
		};

		var innerWidth_max = window.innerWidth
		var innerHeight_max = window.innerHeight

		var onresizeFunction = function ( event ) {
			window.removeEventListener('resize', onresizeFunction)
			var innerWidth_new = window.innerWidth
			var innerHeight_new = window.innerHeight
			if (innerWidth_new < innerWidth_max || innerHeight_new < innerHeight_max) {
				while(true) {
					if (timer) { timer.background() }
					jsPsych.data.writeInteractionData('prompt_open')
					var clave = window.prompt('Por favor, reingrese al modo pantalla completa (F11) y luego ingrese la clave para continuar.')
					if (clave == 'continuar') { 
						if (timer) { timer.foreground() }
						jsPsych.data.writeInteractionData('prompt_close')
						break
					}
				}
			}
			innerWidth_max = Math.max(innerWidth_max, innerWidth_new)
			innerHeight_max = Math.max(innerHeight_max, innerHeight_new)
			window.addEventListener('resize', onresizeFunction)
		}

		// if ( !urlParams.get('ignoreresize') && !mobilecheck() ) {
		// 	window.addEventListener('resize', onresizeFunction)
		// }

		var beforeunload = function(event) {
			saveData()
			// Cancel the event as stated by the standard.
			event.preventDefault();
			// Chrome requires returnValue to be set.
			event.returnValue = '';
		}

		window.addEventListener('beforeunload', beforeunload)

		var saveData = function() {
			// function saveLocal(filename, csvdata) {

			// }

			function saveRecording(filename, audio) {
				var formData = new FormData();
				formData.append('filename', filename);
				formData.append('blob', audio.audioBlob)
				var xhr = new XMLHttpRequest();
				while(true) {
					xhr.open('POST', 'write_audio.php', false);
					try {
						xhr.send(formData);
					} catch(error) {
						console.log(error)
					}
					if(xhr.status == 200) {
						console.log(filename + ' successfully saved to server.')
						break
					} else {
						if (!window.confirm(filename + ' could not be saved to server. Try again?')) {
							audio.download(filename)
							break
						}
					}
				};
			};

			function saveRemote(filename, csvdata) {
				var xhr = new XMLHttpRequest();
				while(true) {
					xhr.open('POST', 'write_data.php', false);
					xhr.setRequestHeader('Content-Type', 'application/json');
					try {
						xhr.send(JSON.stringify({filename: filename, filedata: csvdata}));
					} catch(error) {
						console.log(error)
					}
					if(xhr.status == 200) {
						console.log(filename + ' successfully saved to server.')
						break
					} else {
						if (!window.confirm(filename + ' could not be saved to server. Try again?')) {
							localStorage.setItem(filename, csvdata)
							var blob = new Blob([csvdata], {type: 'text/csv'})
							var a = document.createElement('a')
							var url = URL.createObjectURL(blob)
							a.href = url
							a.download = filename + '.csv'
							document.body.appendChild(a)
							a.click()
							setTimeout(function() {
								document.body.removeChild(a)
								window.URL.revokeObjectURL(url)
							}, 0)
							break
						}
					}
				}
			}

			var basename = uid_string + mode + jsPsych.startTime().toISOString()

			// saveLocal()
			// saveLocal()
			saveRemote('data_' + basename, jsPsych.data.get().ignore('stimulus').csv())
			saveRemote('intdata_' + basename, jsPsych.data.getInteractionData().csv())

			if (recorded) {
				recorded.then(audio => saveRecording('audio_' + basename + '.webm', audio))
			};
		};

		var keydown = function(keystring) {
			// if (window.mobilecheck()) {
			if (true) {
				var e = new Event("keydown")
				e.code=keystring
				document.body.dispatchEvent(e)
				var e = new Event("keyup")
				e.code=keystring
				document.body.dispatchEvent(e)
			}
		}

		var endExperiment = function(event) {
			if(event.key.toLowerCase() == 'q' && event.ctrlKey && event.altKey) {
					if (timer) { 
						timer.clear();
						timer = null;
					};
					removeTimeWarning();
					if (audio_recorder && !recorded) { recorded = audio_recorder.stop() };
					jsPsych.endCurrentTimeline()  // exits instructions loop to avoid confirmation prompt
					jsPsych.pauseExperiment()  // prevent next trial from loading after finishing current trial
					jsPsych.finishTrial()
					setTimeout(saveData, 100)
					jsPsych.endExperiment(`
						<p>La sesión ha finalizado.</p>
						<p style="position:absolute;bottom:0;right:10px;margin:0;font-size:2.5vh;line-height:normal">
							${uid_string}
						</p>
						`
					)
					jsPsych.resumeExperiment()
					window.removeEventListener('keydown', endExperiment)
					window.removeEventListener('resize', onresizeFunction)
					window.removeEventListener('beforeunload', beforeunload)
			}
		}

		window.addEventListener('keydown', endExperiment)

		var uid = urlParams.get('uid')
    	while (true) {
    		if (uid) {
    			if (stimuli[uid-1]) { break }
    		}
    		var uid = window.prompt('uid:', '')    		
		}

		var modes = ['test', 'retest']
		var mode = urlParams.get('mode')
		var record = false;  // do not record audio by default
		var recorded
		while (true) {
			if (mode) {
				if (mode in stimuli[uid-1]) { break }
			}
			let mode_choice = window.prompt('1 (test) / 2 (retest) / 3 (retest w/ mic):', '');
			if (mode_choice == 3) {
				record = true;
				mode_choice = 2;
			};
			mode = modes[mode_choice - 1];
		}

		// only if mode is test, preloads the video file
		var video = 'video/proced.mp4'
		var video_preloaded = false
		if (mode == 'test') {
			var preloadLink = document.createElement("link");
			preloadLink.href = video;
			preloadLink.rel = "preload";
			preloadLink.as = "video";
			preloadLink.setAttribute("onload", "onVideoPreload()");
			document.head.appendChild(preloadLink);
		}

		var stylesheet = document.createElement('style')
	    stylesheet.innerHTML = `#holdscreen {background-color:${mode=='test' ? 'red' : '#AAA'};}`
	    stylesheet = document.head.appendChild(stylesheet)
		var onVideoPreload = function() {
			video_preloaded = true
			stylesheet.innerHTML = '#holdscreen {background-color:#AAA;}'
		}

		var trials = stimuli[uid-1][mode]
		var trial_num = 0
		var max_score = trials.length
		
		var startwith = urlParams.get('startwith')
		if (startwith > 0 && startwith <= trials.length) {
			trials = trials.slice(startwith - 1)	
			trial_num = startwith - 1
		}

		var uid_string = uid.toString().padStart(3, '0')
		var score = 0

		var audio_correct = new Audio('sounds/correct.mp3')
		var audio_wrong = new Audio('sounds/wrong.mp3')
		var audio_win = new Audio('sounds/win.mp3')

    	var fullscreen_on = {
			type: 'fullscreen',
			fullscreen_mode: true,
			data: {},
			delay_after: 0
		}

		var fullscreen_off = {
			type: 'fullscreen',
			fullscreen_mode: false,
			data: {}
		}

		var hold = {
			type: 'html-keyboard-response',
			stimulus: `
				<p id="holdscreen" style="height:100vh;width:100vw;font-size:75vh;margin:0;display:table-cell;vertical-align:middle">
					${uid_string}
				</p>
				`,
			choices: jsPsych.NO_KEYS,
			on_load: function() {
				window.addEventListener('keydown', function _listener(event) {
					if(event.key.toLowerCase() == 's' && event.ctrlKey && event.altKey) {
						if (mode != 'test' || video_preloaded) {
							// if in test mode, hold screen can only be closed if video has been preloaded successfully
							jsPsych.finishTrial( {'key_press': 'ctrl+alt+s'} )
							window.removeEventListener('keydown', _listener)
						} else {
							window.alert(video + ' not preloaded!')
						}
					}
					var last_trial = jsPsych.data.getLastTrialData().values()[0]
					if (last_trial && last_trial.trial_type == 'instructions' &&
						event.key.toLowerCase() == 'r' && event.ctrlKey && event.altKey) {
						jsPsych.finishTrial( {'key_press': 'ctrl+alt+r'} );
						window.removeEventListener('keydown', _listener);
					};
				});
			},
			data: {}
		}

		var known_trial = {
    		type: 'audio-keyboard-response',
    		stimulus: 'sounds/known.mp3',
    		choices: ['KeyS', 'KeyN'],
    		prompt: `
    			<div style="width:100vw;height:100vh;display:table-cell;vertical-align:middle;cursor:auto">
	    			<img src="images/known.png" style="height:25vh" /img><br>
	    			<img src="images/s_key.png" onclick=keydown("KeyS") style="height:25vh;margin:5%" /><img src="images/n_key.png" onclick=keydown("KeyN") style="height:25vh;margin:5%" />
	    			<p style="position:absolute;top:0;right:10px;margin:0;font-size:2.5vh;line-height:normal">${uid_string}</p>
    			</div>
			`,
    		trial_duration: timeout_known*1000,
  			response_ends_trial: true,
  			data: {}
		};

		var video_trial = {
			// set Accept-Ranges header for video/filename.mp4 to none in .htaccess
			// remember to AllowOverride All for Directory in sites-available/site.conf
			// todo: timeout
			// todo: second time, video should not say "push next to proceed, else back to watch again"
			type: 'video',
			sources: ['video/proced.mp4'],
			width: 960,
			height: 545,
			autoplay: true,
			controls: false,
			uid: uid_string,
			max_views: 2,
			key_forward: keystrings.next,
			audio_after: 'sounds/after.mp3',
			decision_timeout: video_decision_timeout*1000,
			// disable_menu: true,
			on_load: function() { removeTimeWarning() },
			data: {}
		}

		var instructions = {
			// set timeout; idem test below (I think it should be continuous, without pauses)
			type: 'instructions',
			pages: [
				makeInstruction({sideA: 'instructions/page1a.jpg', uid: uid_string, isfirst: true}),
				makeInstruction({sideA: 'instructions/page1a.jpg', uid: uid_string, sideB: 'instructions/page1b.jpg'}),
				makeInstruction({sideA: 'instructions/page2a.jpg', uid: uid_string}),
				makeInstruction({sideA: 'instructions/page2a.jpg', uid: uid_string, sideB: 'instructions/page2b.jpg'}),
				makeInstruction({sideA: 'instructions/page3a.jpg', uid: uid_string}),
				makeInstruction({sideA: 'instructions/page3a.jpg', uid: uid_string, sideB: 'instructions/page3b.jpg'}),
				makeInstruction({sideA: 'instructions/page4a.jpg', uid: uid_string}),
				makeInstruction({sideA: 'instructions/page4a.jpg', uid: uid_string, sideB: 'instructions/page4b.jpg'}),
				makeInstruction({sideA: 'instructions/page5a.jpg', uid: uid_string}),
				makeInstruction({sideA: 'instructions/page5a.jpg', uid: uid_string, sideB: 'instructions/page5b.jpg'}),
				makeInstruction({sideA: 'instructions/fin.png', uid: uid_string, islast: true})
			],
			allow_keys: true,
			key_forward: uid >= 500 && uid < 600 ? 'PageDown' : null,
			key_backward: uid >= 500 && uid < 600 ? 'PageUp' : null,
			allow_backward: true,
			show_clickable_nav: false,
			tmin: function() {
				if (jsPsych.data.get().filter({trial_type: 'instructions'}).count()) {
					return 0
				} else { return tmin_instructions }
			},
			data: {},
		}

		var instructions_loop = {
			// loops over the instructions until a researcher enters the key to proceed
			timeline: [instructions, hold],
			loop_function: function() {
				var key_press = jsPsych.data.getLastTrialData().values()[0].key_press
				if (key_press == 'ctrl+alt+s') {
					if (timer) {
						timer.clear()
						timer = null
					}
					removeTimeWarning();
					if (audio_recorder && !recorded) { recorded = audio_recorder.stop() };
					return false
				} else if (key_press == 'ctrl+alt+r') {
					return true
				} else {
					console.log('unexpected key_press value')
				};
				// while(true) {
				// 	if (timer) { timer.background() }
				// 	var clave = window.prompt('Ingrese la clave para continuar:')
				// 	if(clave == 'continuar') {
				// 		if (timer) {
				// 			timer.clear()
				// 			timer = null
				// 		}
				// 		removeTimeWarning()
				// 		return false
				// 	} else { 
				// 		if (timer) { timer.foreground() }
				// 		return true
				// 	}
				// }
			},
			on_load: function() {
				if (!timer) {
					timer = new Timer(function() {
						timer = null
						removeTimeWarning();
						jsPsych.endCurrentTimeline()
						jsPsych.finishTrial()
						while(true) {
							var clave = window.prompt('El tiempo se agotó. Ingrese la clave para continuar:')
							if (clave == 'continuar') { break }
						};
						if (audio_recorder && !recorded) { recorded = audio_recorder.stop() };
					}, timeout_instructions, prewarn)
				};
				if (audio_recorder) { audio_recorder.start() };
			}
		};

		var cards = {
			type: 'html-keyboard-response',
			stimulus: function() {
				trial_num += 1
				return makeHTML({cards: jsPsych.timelineVariable('cards', true), score:score, max_score:max_score, uid:uid, trial_num:trial_num})
			},
			choices: [keystrings.set, keystrings.noset],
			data: { trial_part: 'cards', cards: jsPsych.timelineVariable('cards') },
			on_finish: function(data) {
				data.set_type = jsPsych.timelineVariable('type', true)
				data.set = jsPsych.timelineVariable('type', true) >= 0
				if(data.set) {
					var correct_key = keystrings.set
				} else {
					var correct_key = keystrings.noset
				}
				if (data.key_press) {
					data.response = data.key_press == keystrings.set ? 'set' : 'noset'
					data.correct = data.key_press == correct_key;
				}
			}
		}

		var explanation = {
			type: 'html-keyboard-response',
			stimulus: function() {
				var response = jsPsych.data.getLastTrialData().values()[0].response				
				return makeHTML({cards: jsPsych.timelineVariable('cards', true), response: response, explain: 'ask',
					score: score, max_score: max_score, uid:uid, trial_num:trial_num})
			},
			choices: [keystrings.explain, keystrings.noexplain],
			data: {	trial_part: 'decision', cards: jsPsych.timelineVariable('cards') },
			on_start: function(trial) {
				trial.data.set_type = jsPsych.data.getLastTrialData().values()[0].set_type
				trial.data.set = jsPsych.data.getLastTrialData().values()[0].set
				trial.data.response = jsPsych.data.getLastTrialData().values()[0].response
				trial.data.correct = jsPsych.data.getLastTrialData().values()[0].correct
			},
			on_finish: function(data) {
				if (data.key_press) {
					data.explain = data.key_press == keystrings.explain
				}
				score = jsPsych.data.get().filter({trial_part: 'cards'}).select('correct').sum()
			}
		}

		var feedback = {
			type: 'html-keyboard-response',
			stimulus: function() {
				var correct = jsPsych.data.getLastTrialData().values()[0].correct
				var explain = jsPsych.data.getLastTrialData().values()[0].explain
				var response = jsPsych.data.getLastTrialData().values()[0].response
				var type = jsPsych.data.getLastTrialData().values()[0].set ? 'set' : 'noset'
				return makeHTML({cards: jsPsych.timelineVariable('cards', true), response: response, cards_bg: correct ? 'lawngreen' : 'red',
					explain: explain ? type : 'no', score: score, max_score: max_score, uid:uid, trial_num:trial_num, next:true})
		    },
			choices: [keystrings.next],
			data: { trial_part: 'feedback', cards: jsPsych.timelineVariable('cards')  },
			on_start: function(trial) {
				trial.data.set_type = jsPsych.data.getLastTrialData().values()[0].set_type
				trial.data.set = jsPsych.data.getLastTrialData().values()[0].set
				trial.data.response = jsPsych.data.getLastTrialData().values()[0].response
				trial.data.correct = jsPsych.data.getLastTrialData().values()[0].correct
				trial.data.explain = jsPsych.data.getLastTrialData().values()[0].explain
			},
			on_load: function() {jsPsych.data.getLastTrialData().values()[0].correct ? audio_correct.play() : audio_wrong.play()}
		}

		var test = {
			// set test timeout; as this will be a cutoff for extraordinarily long sessions, I think it shouldn't pause if bathroom, etc
			// if still pause is desired, check stackoverflow 3969475
			timeline: [cards, explanation, feedback],
			timeline_variables: trials,
			on_load: function() {
				if (!timer) {
					timer = new Timer(function() {
						timer = null
						removeTimeWarning()
						jsPsych.endCurrentTimeline()
						jsPsych.finishTrial()
					}, timeout_game, prewarn)
				}
			}
		}

		var winner = {
			type: 'html-keyboard-response',
			stimulus: function() {
				var html = `
					<div style="position:relative;background-color:#ffcc00;height:100vh;width:100vw;display:table-cell;vertical-align:middle">
						<p style="font-size:10vw;margin:0;color:#cc9900">
							¡Ganaste${score > 0 ? ' ' + score + ' punto' : ''}${score > 1 ? 's' : ''}!
						</p>
						<p style="position:absolute;bottom:0;right:10px;margin:0;font-size:2.5vh;line-height:normal">${uid_string}</p>
					`;
				return html
				},
			on_load: function() {
				if (timer) { 
					timer.clear();
					timer = null;
				};
				removeTimeWarning()
				audio_win.play()
				setTimeout(saveData, 100)
				window.removeEventListener('keydown', endExperiment);
				window.removeEventListener('resize', onresizeFunction)
				window.removeEventListener('beforeunload', beforeunload)
			},
			choices: jsPsych.NO_KEYS,
			data: {}
		}

		// var timeline = [fullscreen_on]  // going fullscreen with the fullscreen plugin causes js dialogs to exit fullscreen mode
		var timeline = [hold]
		if (mode == 'retest') {
			timeline.push(instructions_loop)
		} else if (mode == 'test') {
			timeline.push(known_trial)
			timeline.push(video_trial)
		}
		timeline.push(test, winner)

		var preload = [preload_images]
    	if (mode == 'retest') {
    		preload.push(preload_instructions)
    	}
    	preload.push('images/known.png', 'images/s_key.png', 'images/n_key.png')

    	var audio_recorder
    	(async () => {
    		audio_recorder = await recordAudio(record).catch(err => {console.log(err)})  // if promised rejected, catch
		})();
		
		jsPsych.init({
		    timeline: timeline,
		    on_trial_start: function(trial) {
				trial.data.client_ip = client_ip;
				trial.data.navigator = navigator.userAgent;
		    	trial.data.ts_start = Date.now()
		    },
		    on_trial_finish: function(data) {
		    	// var trial_num = data.internal_node_id.split('-').slice(-1)[0].split('.')[1]
		    	data.trial_num = trial_num,
		    	data.ts_end = Date.now()
		    },
		    preload_images: preload,
	      	on_interaction_data_update: function(data) {
    			data.ts_start = Date.now(),
    			data.trial_num = trial_num
  			}
		});

    </script>
</html>
